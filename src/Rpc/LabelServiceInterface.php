<?php 

namespace Khyzd\Contract\Rpc;

interface LabelServiceInterface
{
    /**
     * 初始化所有活动到缓存
     * */
    public function initActivity();

    /**
     * 添加阶梯活动数据到缓存
     * @param $ids 阶梯活动主键数组
     * */
    public function setLadderSpecial(array $ids);

    /**
     * 添加买赠活动数据到缓存
     * @param $ids 买赠活动主键数组
     * */
    public function setBuySwap(array $ids);

    /**
     * 添加品牌满减数据到缓存
     * @param $ids 品牌满减主键数组
     * */
    public function setBrandReduction(array $ids);

     /**
     * 添加专区满减数据到缓存
     * @param $ids 品牌专区满减满折数组
     * */
    public function setShowReduction(array $ids);

    /**
     * 查询商品标签
     * @param $params 商品数组 [['goodsId' => 6, 'brandId' => 15], ['goodsId' => 7, 'brandId' => 8, 'goodsZoneIds' => '1,4']] 
     * @param $userId 用户Id
     * @return array  ['code' => 200, 'data' => ["{$gooods_id}" => [['activity_id' => 1, 'activity_name' => '阶梯特价', 'activity_type' => 'brand_reduction', type => '5', 'discountRate' => '0.01', 'weight' => 100]]]];
     * */
    public function goodsLabel(array $params, int $userId);

}