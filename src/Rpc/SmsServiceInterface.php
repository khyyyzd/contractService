<?php

namespace Khyzd\Contract\Rpc;

/**
 * 短信通知服务
 * 一个方法对应一个短信模版
 * */
interface SmsServiceInterface
{
    /**
     * 发货通知
     * */
    public function deliveryNotice(string $mobile): bool;

    public function qualificationExpirationNotice(string $mobile): bool;    

    /**
     * 葵之云登陆验证码
     * */
    public function kzyLogin(string $mobile): string;

    public function smsCode(string $mobile): string;

    /**
     * 业务员注册审核成功通知
     * @param array $param
     * @return bool
     */
    public function salesmanRegisterApprovalNotice(array $param): bool;

    /**
     * 采购单入库反馈通知
     * @param array $params
     * @return bool
     */
    public function purchaseOrderFeedbackNotice(array $params): bool;

    /**
     * 质管审核通过通知
     * @param array $param
     * @return bool
     */
    public function qualityControlAuditPassedNotice(array $param): bool;

    /**
     * 质管审核驳回通知
     * @param array $param
     * @return bool
     */
    public function qualityControlAuditRejectionNotice(array $param): bool;
}








