<?php

namespace Khyzd\Contract\Rpc;

interface GoodsServiceInterface
{
    /**
     * 套餐组合-商品列表
     * @param $user = ['rank_id' => 2, 'city' => 市编码];
     * */
    public function packageoffer($params = []): array;

    /**
     * 专区-商品列表
     * @param $params ['show_id'] = 专区id;
     * @param $params ['province'] = 省份;
     * */
    public function area_goods($params): array;

    /**
     * 套餐列表
     * @param $user = ['rank_id' => 2, 'city' => 市编码];
     * */
    public function package($params): array;

    /**
     * 专区-买赠-商品列表
     * @param $params ['user_id'] = 用户id;
     * @param $params ['province'] = 省份;
     * @param $params ['rank_id'] = 会员等级
     * @param $params ['city'] = 市
     * @param $params ['district'] = 区
     * @param $params ['review'] = 审核状态
     * */
    public function buySwap(array $params): array;

    /**
     * 专区-品牌-商品列表
     * @param $params ['user_id'] = 用户id;
     * @param $params ['province'] = 省份;
     * @param $params ['rank_id'] = 会员等级
     * @param $params ['city'] = 市
     * @param $params ['district'] = 区
     * @param $params ['review'] = 审核状态
     * */
    public function brand(array $params): array;

    /**
     * 专区-阶梯-商品列表
     * @param $params ['user_id'] = 用户id;
     * @param $params ['province'] = 省份;
     * @param $params ['rank_id'] = 会员等级
     * @param $params ['city'] = 市
     * @param $params ['district'] = 区
     * @param $params ['review'] = 审核状态
     * */
    public function ladder(array $params): array;

    /**
     * @param $params ['user_id'] = 用户id;
     * @param $params ['province'] = 省份;
     * @param $params ['rank_id'] = 会员等级
     * @return array
     */
    public function seckill(array $params): array;

    /**
     * 商品列表
     * @param array $params ['user_id'] = 用户id;
     * @param array $params ['show_id'] = 前端展示专区id;
     * @param array $params ['area_id'] = 商品分类id;
     * @param array $params ['sort_field'] = 排序字段(销量：sales_number 人气：click_count 价格：price);
     * @param array $params ['sort'] = 排序规则:(升降序 asc | desc);
     * @param array $params ['keywords'] = 商品关键词;
     * @param array $params ['step'] = 是否展示促销活动(no_activity | activity);
     * @param array $params ['in_stock'] = 是否有货(yes | no);
     * @param array $params ['smart_purchase'] = 智能采购(历史采购: history 新到货商品: new_goods);
     * @param array $params ['dosage_form'] = 剂型;
     */
    public function goodsList(array $params): array;

    /**
     * 商品新列表
     * @param array $params ['user_id'] = 用户id;
     * @param array $params ['show_id'] = 前端展示专区id;
     * @param array $params ['area_id'] = 商品分类id;
     * @param array $params ['sort_field'] = 排序字段(销量：sales_number 人气：click_count 价格：price);
     * @param array $params ['sort'] = 排序规则:(升降序 asc | desc);
     * @param array $params ['keywords'] = 商品关键词;
     * @param array $params ['step'] = 是否展示促销活动(no_activity | activity);
     * @param array $params ['in_stock'] = 是否有货(yes | no);
     * @param array $params ['smart_purchase'] = 智能采购(历史采购: history 新到货商品: new_goods);
     * @param array $params ['dosage_form'] = 剂型;
     */
    public function goodsNewList(array $params): array;

    /**
     * 商品数量限制列表
     * @param array $params ['current_time'] = 当前时间;
     */
    public function getGoodsNumLimitList(array $params): array;

    /**
     * 商品是否可购买：商品限购数来决定(商品数限制)
     * @param array $params ['user_id] = 用户id
     * @param array $params ['erp_id'] = 商品编号;
     * @param array $params ['num'] = 商品数量;
     * @param array $params ['current_time'] = 当前时间;
     */
    public function canBuy(array $params): array;

    /**
     * 商品限购信息(是否下架以及是否被限购)
     * @param array $params ['user_id] = 用户id
     * @param array $params ['goods_ids'] = 商品id数组;
     */
    public function restrictedPurchaseInfo(array $params): array;

    /**
     * 商品新分类(全部)
     * @return array
     */
    public function newCategory(): array;

    /**
     * 商品新分类下的商品列表
     * @return array
     */
    public function newCategoryGoodsList(array $params): array;

    /**
     * 首页商品专区图标
     * @param array $params ['type'] = 终端类型（1 app,2 pc）
     * @return array
     */
    public function getGoodsZoneList(array $params): array;

    /**
     * 商品楼层
     * @param array $params ['type'] = 终端类型（1 app,2 pc）
     * @return array
     */
    public function getGoodsFloorList(array $params): array;

    /**
     * 获取商品信息
     * @param int $goodsId 商品ID
     * @return array
     */
    public function goodsInfo(int $goodsId): array;

    /**
     * 批量获取商品数据
     * @param array $goodsIdArr
     * @return array
     */
    public function manyGoodsInfo(array $goodsIdArr): array;

    /**
     * 热销推荐商品，根据用户浏览记录推荐商品
     * @param array $param ['userId' => 1,'distributorId' => 11]
     * @return array
     */
    public function recommendGoods(array $param): array;

    /**
     * 商品详情
     * @param array $param ['user_id' => 1,'goods_id' => 1]
     * @return array
     */
    public function detail(array $param): array;

    /**
     * 获取业务员控销商品列表
     * @param array $param ['salesman_id' => 1]
     * @return array
     */
    public function getYwyControlSaleGoodsList(array $param): array;

    /**
     * 获取商品对应的控销用户
     * @param array $param ['goods_id' => 1,'salesman_id' => 1]
     * @return array
     */
    public function getGoodsControlSaleUser(array $param): array;

    /**
     * 设置商品对应的控销用户
     * @param array $param ['goods_id' => 1,'salesman_id' => 1,'user_ids' => '1,2,3']
     * @return array
     */
    public function setGoodsControlSaleUser(array $param): array;

    /**
     * 获取业务员对应的用户
     * @param array $param ['salesman_id' => 1]
     * @return array
     */
    public function getYwyUser(array $param): array;

    /**
     * 获取金刚区
     * @return array
     */
    public function getJingangList(): array;

    /**
     * 检查用户该商品的经营范围
     * @param array $param ['goods_id' => 1,'user_jyfw_ids'=>[1,2,3]]
     * @return bool
     */
    public function checkGoodsType(array $param): bool;

    /**
     * 清除商品缓存数据
     * @param array $goodsIds 商品 ID
     * @return void
     */
    public function clearGoodsCache(array $goodsIds): void;

    /**
     * 获取商品信息
     * @param int $userId 用户id
     * @param array $goodsIds 商品ids
     * @return array
     */
    public function getGoodsInfoByIds(int $userId, array $goodsIds): array;

    /**
     * 常购清单
     * @param int $userId
     * @param int $page
     * @param int $row
     * @return array
     */
    public function purchase(int $userId, int $page, int $row): array;

    /**
     * 限购：获取不能购买的商品信息
     * @param int $userId
     * @param array $goodsIds
     * @return array
     */
    public function getGoodsRestrictedPurchaseInfo(int $userId, array $goodsIds): array;
}








