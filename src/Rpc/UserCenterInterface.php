<?php
declare(strict_types = 1);

namespace Khyzd\Contract\Rpc;

use Exception;

interface UserCenterInterface
{
    /**
     * 用户行为统计
     * @param int $userId
     * @param array $behaviorData
     * @param array $behaviorData
     *
     * @return array
     */
    public function behaviorStatistics(int $userId,array $behaviorData):array;

    /**
     * 用户信息
     * @param int $id 用户ID
     * @return array 用户信息
     */
    public function userInfo(int $id): array;

    /**
     * 获取用户配送方案
     * @param int $userId 用户ID
     * @return array 用户所属配送方案
     */
    public function userDistributionPlan(int $userId):array;

    /**
     * 资质认证
     * @param int $userId 用户ID
     * @param array $qualification 资质数据
     * @return array
     */
    public function accreditation(int $userId,array $qualification):array;

    /**
     * 新版用户行为记录（异步队列）
     * @param array $behaviorRecord 用户行为数据
     * @return array
     */
    public function userBehavior(array $behaviorRecord):array;

    /**
     * 查询用户资质证照（七彩云和）
     * @param int $userId 用户ID
     * @return array 用户在七彩云和上的资质证照
     * @throws Exception
     */
    public function getLicenseByColorful(int $userId): array;

    /**
     * 保存用户资质证照数据
     * @param int $userId 用户ID
     * @param array $data 资质证照数据
     * @return void
     * @throws Exception
     */
    public function saveLicense(int $userId, array $data): void;

    /**
     * 获取用户资质信息
     * @param int $userId 用户ID
     * @return array
     */
    public function getQualification(int $userId): array;

    /**
     * 实名认证（七彩云和）
     * @param int $userId 用户ID
     * @return array 实名认证结果
     * @throws Exception
     */
    public function realNameByColorful(int $userId): array;

    /**
     * 上传资质至七彩云和
     * @param int $userId 用户ID
     * @return array
     * @throws Exception
     */
    public function uploadQualByColorful(int $userId): array;

    /**
     * 首营交换
     * @param int $userId 用户ID
     * @param string $certId 资料ID JSON字符串
     * @return array
     * @throws Exception
     */
    public function signatureByColorful(int $userId, string $certId): array;

    /**
     * 保存用户资质证照数据（V2）
     * @param int $userId 用户ID
     * @param array $data 资质证照数据
     * @return void
     * @throws Exception
     */
    public function saveLicenseV2(int $userId, array $data): void;

    /**
     * 获取用户资质时效数据
     * @param int $userId 用户ID
     * @return array 返回资质时效数据
     */
    public function getLicense(int $userId): array;

    /**
     * 获取用户经营范围
     * @param int $userId 用户 ID
     * @param string $field 字段名
     * @return array 返回用户经营范围
     */
    public function getBusinessScope(int $userId, string $field = 'jyfw_name'): array;

    /**
     * 获取用户配送方案
     * @param int $userId
     * @return array
     */
    public function getDistributionPlan(int $userId): array;

    /**
     * 确认账号状态
     * @param int $userId
     * @return void
     * @throws Exception
     */
    public function deeterminingAccountStatus(int $userId): void;

    /**
     *  获取用户注册功能开启状态
     * @return array
     */
    public function getUserRegisterStatus(): array;

    /**
     * 获取用户收货地址
     * @param int $userId 用户 ID
     * @return array
     */
    public function getShippingAddress(int $userId): array;

    /**
     * 获取用户账户信息
     * @param int $userId 用户 ID
     * @return array
     */
    public function getUserAccount(int $userId): array;

    /**
     * 获取用户绑定的业务员 ID
     * @param int $userId 用户 ID
     * @return array
     */
    public function getSalesmanIds(int $userId): array;

    /**
     * 添加客户账户变动记录
     * @param array $params
     * @return void
     */
    public function addAccountRecord(array $params): void;

    /**
     * 更新用户基本信息
     * @param int $userId 用户 ID
     * @param array $data 更新数据
     * @return void
     */
    public function updateUserInfo(int $userId, array $data): void;
//    /**
//     * 根据条件获取用户信息
//     * @param array $condition
//     * @return array
//     */
//    public function getUserInfoByCondition(array $condition): array;
//
//    /**
//     * 获取用户等级
//     * @param int $rankId
//     * @return array
//     */
//    public function getRankInfo(int $rankId): array;
//
//    /**
//     * 获取用户资质
//     * @param int $userId
//     * @return array
//     */
//    public function getCampInfo(int $userId): array;
//
//    /**
//     * 账号密码登录
//     * @param array $params
//     * @return array
//     */
//    public function accountLogin(array $params):array;



    

}