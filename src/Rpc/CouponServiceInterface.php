<?php 

namespace Khyzd\Contract\Rpc;

interface CouponServiceInterface
{
    /**
     * 业务员优惠券列表
     * */
    public function admin_list(int $salesmanId);
    
    /**
     * 业务员优惠券发放
     * @param  $param = ['youhq_id' => 优惠券id, 'admin_id' => '业务员id', 'user_ids' => [1,2,3]用户ids];
     * */
    public function admin_send($param);

    /**
     * 业务员优惠券发放
     * @param  $param = ['admin_id' => '业务员id', 'sdate' => '2022-08-01', 'edate' => '2022-08-31'];
     * */
    public function admin_send_log($param);

}