<?php

namespace Khyzd\Contract\Rpc;

/**
*支付相关接口
* */
interface PayServiceInterface
{

        /**
     * 给三方支付下单
     * */
    public function placeOrder(array $params): array;

    /**
     * 支付回调修改订单信息
     * */
    public function orderNotify(array $returnData): string;
    

}








