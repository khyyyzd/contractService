<?php

namespace Khyzd\Contract\Rpc;

interface PaymentServiceInterface
{
    /**
     * 支付
     * @param string $channel 支付渠道标识
     * @param array $data 订单支付数据
     * @return array
     */
    public function pay(string $channel, array $data): array;

    /**
     * 回调通知
     * @param string $channel 支付渠道标识
     * @param array $data 支付回调通知数据
     * @return string
     */
    public function callback(string $channel, array $data): array;

    /**
     * 取消支付
     * @param string $channel 支付渠道标识
     * @param int $orderId 订单 ID
     * @return bool
     */
    public function cancel(string $channel, int $orderId): bool;
}