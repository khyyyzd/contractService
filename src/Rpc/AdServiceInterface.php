<?php 

namespace Khyzd\Contract\Rpc;

interface AdServiceInterface
{
    /**
     * @param $posId  广告位id required
     * @param $userId 用户id 需要登陆的广告位必传
     * */
    public function list(int $posId = 0, int $userId = 0): array;

    /**
     * 根据广告位置分类获取广告
     * @param string $position 广告位置分类，HomePage：首页、SearchPage：搜索页
     * @param int|null $userId 用户ID，默认0：未登录
     * @return array
     */
    public function classifiedList(string $position, ?int $userId):array;

    /**
     * 跑马灯
     * @return array
     */
    public function marquee():array;
}