<?php
declare(strict_types = 1);

namespace Khyzd\Contract\Rpc;

/**
 * 购物车服务契约
 */
interface ShoppingCartInterface
{
    /**
     * 获取购物车商品列表（V1）
     * @param array $userInfo
     * @return array
     */
    public function list(array $userInfo):array;

    /**
     * 获取购物车商品列表
     * @param int $userId
     * @return array
     * @throws
     */
    public function listV2(int $userId): array;

    /**
     * 购物车添加商品（V1）
     * @param int $userId 用户ID
     * @param array $goodsData 商品数据
     * @return array
     */
    public function add(int $userId,array $goodsData):array;

    /**
     * 添加购物车（V2）
     * @param int $userId 用户 ID
     * @param array $goodsData 商品数据
     * @return void
     * @throws
     */
    public function addV2(int $userId,array $goodsData): void;

    /**
     * @param array $userInfo // 用户信息
     * @param int $goodsId  // 商品ID
     * @param string $op    // 加减操作，incr增加，minus减少
     * @param int $num  // 数量
     * @param string $activityId // 活动标识
     * @return array
     */
    public function edit(array $userInfo, int $goodsId, string $op,int $num,string $activityId, int $packGoodsId, int $skId):array;

    /**
     * 编辑购物车（V2）
     * @param int $userId 用户 ID
     * @param array $goodsData 商品数据
     * @return array
     * @throws
     */
    public function editV2(int $userId,array $goodsData): array;

    /**
     * @param int $userId
     * @param array $goodsData
     * @param int $goodsData.goodsId 商品ID
     * @param string $goodsData.activityId 活动标识
     * @param string $goodsData.packGoodsId 套餐组合内商品ID
     * @return array
     */
    public function delete(int $userId,array $goodsData):array;

    /**
     * 删除购物车（V2）
     * @param int $userId 用户 ID
     * @param array $goodsData 商品数据
     * @return void
     * @throws
     */
    public function deleteV2(int $userId,array $goodsData): void;

    /**
     * @param int $userId
     * @param int $op   全选：1，单选：0
     * @param array $goodsData
     * @return array
     */
    public  function selected(int $userId,int $op,array $goodsData):array;

    /**
     * 购物车选中商品（V2）
     * @param int $userId 用户 ID
     * @param array $params option 0：单选、1：全选、2：反选
     * @return void
     * @throws
     */
    public function select(int $userId, array $params): void;
}