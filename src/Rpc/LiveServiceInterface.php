<?php

namespace Khyzd\Contract\Rpc;

/**
 *直播相关接口
 * */
interface LiveServiceInterface
{

    /**
     * 直播列表
     * */
    public function getLiveList(array $params): array;

    /**
     * 直播商品列表
     * */
    public function getLiveGoods(array $params): array;


}








