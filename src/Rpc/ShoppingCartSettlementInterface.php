<?php
declare(strict_types = 1);

namespace Khyzd\Contract\Rpc;

/**
 * 购物车结算服务契约
 */
interface ShoppingCartSettlementInterface
{
    /**
     * 购物车实时结算实际支付金额
     * @param array $userInfo 用户信息
     * @param $userInfo user_id
     * @param #userInfo province 
     * @param $userInfo city
     * @param $userInfo rank_id
     * */
    public function settlement(array $userInfo):array;
}