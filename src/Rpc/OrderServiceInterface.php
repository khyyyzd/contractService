<?php

namespace Khyzd\Contract\Rpc;

use Exception;

interface OrderServiceInterface
{
    /**
     * 确认订单（V2）
     * @param array $additionalData 附加数据（灵活换购商品数据...）
     * @return array
     */
    public function confirmV2(array $additionalData): array;

    /**
     * 确认订单（V3）
     * @param int $userId 用户 ID
     * @param array $additionalData 附属数据（灵活换购商品数据...）
     * @return array
     */
    public function confirmV3(int $userId,array $additionalData): array;

    /**
     * 提交订单
     * @param int $userId 用户ID
     * @param int $hasInfo 是否携带纸质首营
     * @param string $remark 备注信息
     * @param string $coupon 使用优惠券ID，多个以逗号分隔
     * @param int $source 来源标识，1:用户APP，2:业务员APP，0:PC端
     * @param string $discountDetails 优惠明细json字符串
     * @return array
     */
    public function submit(int $userId, int $hasInfo, string $remark, string $coupon, int $source, string $discountDetails): array;

    /**
     * 提交订单（V2）
     * @param array $params
     * @return array
     */
    public function submitV2(array $params): array;

    /**
     * 订单详情
     * @param int $userId 用户ID
     * @param int $orderId 订单ID
     * @return array
     */
    public function details(int $userId, int $orderId): array;

    /**
     * 再来一单
     * @param int $userId 用户ID
     * @param int $orderId 订单ID
     * @return array
     */
    public function another(int $userId, int $orderId): array;

    /**
     * 退货订单更新快递凭证
     * @param array $voucher.rid 退货订单ID
     * @param array $voucher.userId 用户ID
     * @param array $voucher.url 快递凭证图片地址
     * @return array
     */
    public function courierVoucher(array $voucher): array;

    /**
     * 订单活动记录
     * @param string $orderNum 订单号
     * @param int $orderStatus 订单状态，默认为5
     * @return array
     */
    public function activityRecord(string $orderNum,int $orderStatus = 5):array;

    /**
     * 更新订单支付状态（订单支付回调通知）
     * @param array $notifyData
     * @return bool
     */
    public function updatePaymentStatus(array $notifyData): bool;

    /**
     * 取消订单
     * @param array $params
     * @return void
     */
    public function cancel(array $params): void;

    /**
     * 更新订单退货状态
     * @param string $orderNumber
     * @param int $refundStatus
     * @return void
     * @throws Exception
     */
    public function updateRefundStatus(string $orderNumber, int $refundStatus): void;

    /**
     * 发送订单状态 MQ 消息
     * @param string $orderNumber 订单号
     * @param int $orderStatus 订单状态，1：待付款、3：待发货、4：待收货、5：已完成
     * @return void
     */
    public function sendOrderStatusMqMessage(string $orderNumber, int $orderStatus): void;

    
}