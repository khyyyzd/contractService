<?php 

namespace Khyzd\Contract\Rpc;

/**
 * 过度页接口
 * */
interface IndexServiceInterface
{
	/**
     * 获取过度页信息
     * @return array 返回数据
     */
    public function info(): array;

}