<?php
declare(strict_types = 1);

namespace Khyzd\Contract\Rpc;

interface NewsServiceInterface
{
    /**
     * @param int $cid 分类ID
     * @return array 返回新闻列表，示例：[['art_id' => 新闻ID, 'name' => '测试消息标题', 'cover_url' => '新闻图片', 'cover_link' => '跳转链接', 'video' => '视频地址', 'is_flag' => '是否加标签', 'create_time' => '创建时间']]
     */
    public function newsList(int $cid):array;
}