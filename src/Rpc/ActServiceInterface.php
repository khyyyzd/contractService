<?php 

namespace Khyzd\Contract\Rpc;

use Exception;

interface ActServiceInterface
{
    /**
     * 阶梯特价
     * @param user= ['user_id' => 485, 'city' => '140100', 'rank_id' => '2']; 
     * @param goods = [['goods_id' => 商品id, 'num' => 购买数量, 'price' => 商品原价]]  
     */
    public function ladder($user = [], $goodslist = []); //阶梯特价
    
    /**
     * 买赠活动
     * @param user= ['user_id' => 485, 'city' => '市', 'rank_id' => '2']; 
     * @param goodslist = [['goods_id' => 商品id, 'num' => 购买数量, 'price' => 商品原价]]  
    */
    public function buyswap($user = [], $goodslist = []);

    /**
     * @param user= ['user_id' => 485, 'city' => '市', 'rank_id' => '2'];
     * @param list=[['brand_id' => '品牌id', 'amount' => '优惠后总金额']];
     * */
    public function brand($user = [], $list = []);

    /**
     * 套餐数量
     * @param user= ['user_id' => 485, 'city' => '市', 'rank_id' => '2'];
     * @param list=[['package_id' => '套餐id', 'num' => '套餐数量']] 
     * */
    public function package($user = [], $list = []);

    /**
     * 打包优惠
     * @param user= ['user_id' => 485, 'city' => '市', 'rank_id' => '2'];
     * @param list= ['$打包优惠id' => [['goods_id' => 商品id, 'num' => 商品数量, 'price' => 商品价格]]]
     * */
    public function packageoffer($user = [], $list = []);

    /**
     * 全场优惠
     * @param user= ['user_id' => 485, 'city' => '市', 'rank_id' => '2'];
     * @param $list = ['amount' => 总金额];
     * @return ['amount' => 总金额, 'discount_amount' => 优惠金额];
     * */
    public function discount($user = [], $list = []);

    /**
     * 可用优惠券列表
     * @param user= ['user_id' => 485, 'city' => '市', 'rank_id' => '2']; 
     * @param $goods = [['goods_id' => 商品id, 'num' => 下单数量, 'price' => 原价 ]] 不要传套餐商品和打包优惠商品; 
     * $amount = 0 //优惠后商品总金额
     **/
    public function coupon_list($user = [], $goods = [], $amount = 0);
    
    /**
     * 获取标签 
     * @param $user = ['user_id' => 用户id, 'city' => 城市, rank_id => ‘2’];
     * @param $info = ['goods_id' => 商品id];
     * */
    public function label_msg($user = [], $info = []);

    /**
     * 秒杀服务计算优惠金额
     * @param array $goodsList = [['seckillId' => '秒杀活动id', 'goodsId' => '商品id', 'num' => '秒杀数量', 'price' => '商品原价']];
     * */
    public function seckill_service(array $goodsList): array;

    /**
     * 用户是否参加全场特惠购
     * @param array $user;
     * @param int $user user_id;
     * @param string $user city; //城市
     * @param string $user rank_id //会员等级
     * */
    public function hasFullActivity(array $user): array;

    /**
     * 全场特惠购计算
     * @param array $user;
     * @param int $user user_id;
     * @param string $user city; //城市
     * @param string $user rank_id //会员等级
     * @param array $info;
     * @param int $info amount; //实际支付金额
     * */
    public function fullActivity(array $user, array $info): array;

    /**
     * 专区满减满折
     * @param user= ['user_id' => 485, 'city' => '市', 'rank_id' => '2'];
     * @param list= [['show_id' => '专区id', 'goods' => [['goods_id' => 1, 'price' => 15.00, 'num' => 10]]]] 
     * */
    public function showReduction(array $user = [], array $list = []): array;

    /**
     * 全场折上折
     * @param user= ['user_id' => 485, 'province' => '省', 'rank_id' => '2'];
     * @param info= ['amount' => 1000]; 
     * */
    public function foldUp(array $user, array $info): array;

    /**
     * 套餐限购查询
     * @param info= ['user_id' => 485, 'package_id' => '套餐id', 'num' => '套餐数量'];
     * @return $result = [
     *      'code' => 200,
     *      'msg' => [
     *          'user_id' => 485,
     *          'package_id' => 1
     *          'num' => 10,
     *          'limitNum' => 0, //限购数量  0不限购
     *          'limitResult' => false, //限购结果 false表示未达到限制要求 true表示达到限制要求(不能购买)
     *          'canBuyNum' => 0, 还能购买数量
     *      ],
     * ]
     * */
    public function packageLimitQuery(array $info): array;

    /**
     * 支付成功后给大传盘增加抽奖次数
     * @param $orderNumber 订单号
     * @return 错误信息 
     * */
    public function turntableAddTimes(string $orderNumber): string;

    /**
     * 灵活换购
     * @param array $user;
     * @param int $user user_id;
     * @param string $user city; //城市
     * @param string $user rank_id //会员等级
     * @param array $goodsList [['goods_id' => 1, 'price' => '13.20', 'num' => 10]];
     * @return array $result = [
     *      'code' => 200,
     *      'data' => [
     *          'flag' => 1,
     *          'differAmount' => 0,
     *          'times' => 1,
     *          'flexibleExchangeId' => 1,
     *          'tips' => '提示文案提示文案提示文案',
     *          'way' => 0,
     *          'goods' => [
     *              [
     *                  'flexibleExchangeRuleGoodsId' => 1,
     *                  'flexibleExchangeId' => 1,
     *                  'flexibleExchangeRuleId' => 1,
     *                  'goodsId' => 5,
     *                  'goodsName' => '克感利咽颗粒',
     *                  'maxNum' => 10,
     *                  'price' => '0.1',
     *              ] 
     *          ],
     *      ],
     *  ];
     * */
    public function flexibleExchange(array $user, array $goodsList): array;


    /**
     * 计算阶梯特价优惠金额
     * @param $goods = ['goods_id' => '商品ID', 'num' => '数量', 'price' => '原价', 'activity_id' => '活动ID'];
     * @return $result = ['code' => 200, 'data' => $array];
     *         $array = [
     *              'goods_id' => '商品ID', 
     *              'num' => '数量',
     *              'price' => '原价'
     *              'ladder_amount' => '阶梯优惠金额', 
     *              'ladder_price' => '优惠单价',
     *         ];
     * */  
    public function ladderInfo(array $goods): array;

    /**
     * 计算买赠优惠金额
     * @param $goods = ['goods_id' => '商品ID', 'num' => '数量', 'price' => '原价', 'activity_id' => '活动ID'];
     * @param $user= ['user_id' => 485];
     * @return $result = [
     *                      'code' => 200, 
     *                      'data' => [
     *                          'buyswap_quantity' => 赠送数量, 
     *                          'discount_price' => '折后单价',
     *                          'discount_amount' => '优惠总金额',
     *                      ],
     *                   ];
     * */
    public function buySwapInfo(array $goods, array $user): array;

    /**
     * 计算品牌优惠金额
     * @param $goodsList = [['goods_id' => '商品ID', 'num' => '数量', 'price' => '原价']];
     * @param $activity_id = 'tb_brand_reduction表主键'
     * @return ['discount_amount' => 优惠总金额, 'goods_list' => [['goods_id' => '商品ID', 'num' => '数量', 'price' => '原价', 'discount_price' => '折后单价']]];
     * */
    public function brandInfo(array $goodsList, int $activity_id): array;

    /**
     * 查询套餐优惠
     * @param params= ['package_id' => '套餐id', 'num' => '套餐数量']
     * @return ['code' => 200, 'data' => $result]
     * $result = ['package_id' => '套餐id', 'num' => '套餐数量', 'goods' => [['goods_id' => '商品ID', 'num' => '套餐内单个商品数量', 'discount_price' => '优惠单价', 'original_price' => '原价']], 'discount_amount' => '优惠总金额', 'total_amount' => '单个套餐原价', 'all_total_amount' => '全部套餐原价', 'dis_amount' => '单个套餐价', 'all_dis_amount' => '全部套餐价'];
     * */
    public function packageInfo(array $params): array;

    /**
     * 获取秒杀活动
     * @param int $userId 用户 ID
     * @return array
     * @throws Exception
     */
    public function getSeckillActivity(int $userId): array;

    /**
     * 获取指定秒杀活动
     * @param int $seckillId
     * @return array
     */
    public function getSeckill(int $seckillId): array;

    /**
     * 递增更新秒杀商品抢购数量
     * @param int $seckillId 秒杀活动 ID
     * @param int $goodsId 商品 ID
     * @param int $robbedNumber 抢购数量
     * @return void
     */
    public function updateSeckillRushPurchaseNumber(int $seckillId, int $goodsId, int $robbedNumber): void;
    



    





    
}








