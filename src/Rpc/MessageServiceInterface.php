<?php
declare(strict_types = 1);

namespace Khyzd\Contract\Rpc;

interface MessageServiceInterface
{
    /**
     * 发送消息通知
     * @param array $messageData 消息推送的数据
     * @param string $messageData.msgType 消息类型
     * @param string $messageData.title 消息标题
     * @param string $messageData.description 消息简介
     * @param string $messageData.content 消息详情内容
     * @param string $messageData.picUrl 消息缩略图
     * @param string $messageData.notifyChannel 通知渠道
     * @param string $messageData.notifyTime 通知时间，空立即发送
     * @param string $messageData.link 消息打开页面链接
     * @param string $messageData.linkExpires 跳转链接失效时间
     * @param string $messageData.pushType 推送类型，1：全部（群推）、2：批量（cid批量推）、3：指定（cid批量推）、4：单推（cid单推）
     * @param string $messageData.pushUserRank Push推送的用户类型
     * @param string $messageData.pushProvince Push推送的省份
     * @param string $messageData.pushUsers Push推送的指定用户ID
     * @return array 发送结果
     */
    public function sendMessage(array $messageData):array;

    /**
     * 获取消息类型
     * @param int|null $userId 用户ID，默认null
     * @return array 返回消息类型数据/用户已读总数
     */
    public function getMsgType(?int $userId):array;

    /**
     * 获取通知消息列表
     * @param int $msgType 消息类型
     * @param int $userId 用户ID
     * @return array 返回通知消息列表
     */
    public function getMsgList(int $msgType,int $userId):array;

    /**
     * 获取消息详情
     * @param int $mgsId 消息ID
     * @return array 返回消息详情
     */
    public function getMsgDetails(int $msgId):array;

    /**
     * 全部已读
     * @param int $userId 用户ID
     * @return array 返回成功失败
     */
    public function msgAllRead(int $userId):array;


    /**
     *  葵之云消息推送
     * @param array $messageData
     * @return array
     */
    public function kzySendMessage(array $messageData):array;
}