<?php
declare(strict_types = 1);

namespace Khyzd\Contract\Rpc;

interface OcrServiceInterface
{
    /**
     * @param array $param.url 文件地址
     * @param array $param.cartType  证件类型，1：营业执照、2：身份证，3：医疗许可证，4：采购、收货委托书
     * @param array $param.side  身份证正反面，front：身份证人像面，back：身份证国徽面
     * @return array 返回识别的文字
     */
    public function identify(array $param):array;
}