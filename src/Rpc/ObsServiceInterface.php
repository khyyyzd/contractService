<?php 

namespace Khyzd\Contract\Rpc;

interface ObsServiceInterface
{
    public function upload(array $params): string;

    public function apiUpload(array $params): string;

    /**
     * 给图片生成哈希值(指纹)
     * @param $path（图片地址）
     * @return $result string  64位二进制值
     * */
    public function createImageHash(string $path): string;

    /**
     * 查找是否有相似图片
     * @param $hashValue1 图片指纹（64位二进制值 ）
     * @param $hashValueArray 图片指纹数组 
     * @return $result string  64位二进制值 (有值代表相同或相似的图片)
     * */
    public function sameImages(array $hashValueArray1, array $hashValueArray2): array;















    
}